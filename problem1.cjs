// function to find details of car with id 33

function problem1(inventory, search_id){
    if((inventory === undefined) || !Array.isArray(inventory) || inventory.length==0 ) {
        return []
      }
    if (search_id === undefined || !Number.isInteger(search_id)){
        return []
    }
    for (i=0; i<inventory.length; i++){
        car = inventory[i]
        if (car.hasOwnProperty("id") && car.id === search_id){
            return car
            }
    }
    return []
}

module.exports = problem1

