// function to find the make and model of the last car in the inventory

function problem2(inventory) {
    if((inventory === undefined) || !Array.isArray(inventory) || inventory.length==0 ) {
        return []
      }
    let last_car = inventory[inventory.length-1]
    if (last_car.hasOwnProperty("car_make") && last_car.hasOwnProperty("car_model")){
        return [last_car.car_make , last_car.car_model]
    }
    
    
}

module.exports = problem2 

