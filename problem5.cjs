// function to find the no of cars which are older than the year 2000

function problem5(yearList) {
    if((yearList === undefined) || !Array.isArray(yearList) || yearList.length==0 ) {
        return []
      }
    let yearOlderThan2000 =[];
    for (i=0; i<yearList.length; i++){
        if (yearList[i]<2000){
            yearOlderThan2000.push(yearList[i])
        }
    }
    return yearOlderThan2000
}

module.exports = problem5


