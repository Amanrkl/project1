// function to sort all the car model names into alphabetical order

function problem3(inventory) {
    if((inventory === undefined) || !Array.isArray(inventory) || inventory.length==0 ) {
        return []
      }
    inventory.sort((car1, car2) => {
        const model1 = car1.car_model.toUpperCase(); // ignore upper and lowercase
        const model2 = car2.car_model.toUpperCase(); // ignore upper and lowercase

        if (model1 < model2) {
            return -1;
        }
        if (model1 > model2) {
            return 1;
        }

        // model names must be equal
        return 0;
        });

    return inventory
}

module.exports = problem3


