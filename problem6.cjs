// function to return an array that only contains BMW and Audi cars

function problem6(inventory){
    if((inventory === undefined) || !Array.isArray(inventory) || inventory.length==0 ) {
        return []
      }
    let BMWAndAudi =[]
    for (i=0; i<inventory.length; i++){
        car = inventory[i]
        if (car.car_make == "BMW" || car.car_make == "Audi"){
            BMWAndAudi.push(car);
        }
    }
    return BMWAndAudi
}

module.exports = problem6 

