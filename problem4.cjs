// function to return an array from the dealer data containing only the car years

function problem4(inventory) {
    if((inventory === undefined) || !Array.isArray(inventory) || inventory.length==0 ) {
        return []
      }
    let yearList =[];
    for (i=0; i<inventory.length; i++){
        car=inventory[i]
        yearList.push(car.car_year)
    }

    return yearList
}

module.exports = problem4 


