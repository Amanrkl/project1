let {inventory} = require('../inventory.cjs')
let problem2 = require('../problem2.cjs')

const [make, model] = problem2(inventory);

console.log(`Last car is a ${make} ${model}`);
